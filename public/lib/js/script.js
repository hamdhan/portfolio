const scrollContainer = document.getElementById('scroll-container');

    scrollContainer.addEventListener('wheel', (evt) => {
      if (evt.deltaY > 0) {
        scrollContainer.scrollBy({ left: window.innerWidth, behavior: 'smooth' });
      } else {
        scrollContainer.scrollBy({ left: -window.innerWidth, behavior: 'smooth' });
      }
    });